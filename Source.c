#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ranNum.h"
#include<limits.h>

#define n 	1000	//no pf particle
#define tStart	0.0
#define tEnd	5.0
#define timeStep	0.01
#define	systemWidth	100	//size of the system
#define	systemHeight	100		//size of the system
#define	sigma	1.0
#define	maxDisplacement	sigma/2		//max displacement
#define vInitial	10		//define max initial valocity (may need a larger value for initial velocity)
#define ac 0.0

struct Particle{
	double xPos;
	double yPos;
	double xVel, yVel;
	double xAccPast, xAccPresent, yAccPast, yAccPresent;
};

typedef struct Particle Particle;


//function to randonly displace particles initially
void displaceRandom(Particle particles[n]){
	int i;
	double tempRandom;


	for (i = 0; i < n; i++){

		tempRandom = randNum();
		particles[i].xPos = particles[i].xPos + 2.0*(tempRandom - 0.5) * maxDisplacement;

		// to ensure circular boundary conditions
		while (particles[i].xPos<0.0){
			particles[i].xPos = systemWidth + particles[i].xPos;
		}
		while (particles[i].xPos >= systemWidth){
			particles[i].xPos = particles[i].xPos - systemWidth;
		}

		tempRandom = randNum();
		particles[i].yPos = particles[i].yPos + 2.0*(tempRandom - 0.5) * maxDisplacement;

		// to ensure circular boundary conditions
		while (particles[i].yPos<0.0){
			particles[i].yPos = systemHeight + particles[i].yPos;
		}
		while (particles[i].xPos >= systemHeight){
			particles[i].yPos = particles[i].yPos - systemHeight;
		}


		//random direction given to velocities
		tempRandom = randNum();
		particles[i].xVel = 2.0*(tempRandom - 0.5)*vInitial;
		tempRandom = randNum();
		particles[i].yVel = 2.0*(tempRandom - 0.5)*vInitial;

	}

}

double min(double a, double b){
	if (abs(a) > abs(b))
		return b;
	return a;
}

//to calculate force on each particle and hence acceleration
void acc(Particle particles[n]){

	int i, j;
	double a, b, c, d;
	double axsum, aysum, xx, yy, rr, rt;
	double sin, cos;


	for (i = 0; i < n; i++){
		axsum = 0.0; aysum = 0.0;

		for (j = 0; j < n; j++){

			if (i == j){
				continue;
			}

			//take care of the circular boundary conditions carefully
			else{
				xx = particles[i].xPos - particles[j].xPos;

				xx = min(xx, systemWidth - xx);

				yy = particles[i].yPos - particles[j].yPos;

				yy = min(yy, systemHeight - yy);


			}
			rr = (xx*xx) + (yy*yy);

			//just to ensure that the value doesn't show unwanted behaviour while ronding off.
			//just a trick to avoid nasty behaviour ;)
			if (rr < 0.8)
				rt = 0.8;
			else if (rr > 1000000)
				rt = 4;
			else
				rt = sqrt(rr);

			if (rt > 3.0){
				continue;
			}

			sin = yy / rt;
			cos = xx / rt;

			a = pow(1 / rt, 13.0); b = pow(1 / rt, 7.0); c = (2.0 * a) - (1.0 * b); d = 24.0*c;

			axsum = axsum + (d*cos);
			aysum = aysum + (d*sin);
			//	printf("acc %lf %lf\n", axsum, aysum);
		}
		particles[i].xAccPresent = axsum;
		particles[i].yAccPresent = aysum;
		//	printf("final acc %lf %lf\n", axsum, aysum);
		//getchar();
	}

}

//Verlet algorithm for the position
void pos(Particle particles[n]){
	int i;

	for (i = 0; i < n; i++){

		particles[i].xPos = particles[i].xPos + timeStep*particles[i].xVel + 0.5*timeStep*timeStep*particles[i].xAccPast;

		//enforcing circular boundary conditions
				if(particles[i].xPos>systemWidth){
            int temp=(int)particles[i].xPos;
            int mod=temp%systemWidth;
            particles[i].xPos=(particles[i].xPos-temp)+mod;

		}

		if(particles[i].xPos<0.0){
            int temp=(int)particles[i].xPos+1;
            int mod=temp%systemWidth;
            particles[i].xPos=(particles[i].xPos-temp)+mod+systemWidth;
		}

		particles[i].yPos = particles[i].yPos + timeStep*particles[i].yVel + 0.5*timeStep*timeStep*particles[i].yAccPast;

		//enforcing circular boundary conditions
			if(particles[i].yPos>systemHeight){
            int temp=(int)particles[i].yPos;
            int mod=temp%systemHeight;
            particles[i].yPos=(particles[i].yPos-temp)+mod;

		}

		if(particles[i].yPos<0.0){
            int temp=(int)particles[i].yPos+1;
            int mod=temp%systemHeight;
            particles[i].yPos=(particles[i].yPos-temp)+mod+systemHeight;
		}
	}
}



//verlet algorithm for velocity
void vel(Particle particles[n]){
	int i;

	for (i = 0; i < n; i++){
		particles[i].xVel = particles[i].xVel + 0.5*timeStep*(particles[i].xAccPresent + particles[i].xAccPast);
		particles[i].yVel = particles[i].yVel + 0.5*timeStep*(particles[i].yAccPresent + particles[i].yAccPast);

	}
}


//initializes the problem by distributing the particles uniformly
void initialize(Particle particles[n]){
	double i, j;

	double areaFactor = (double)(systemWidth *systemHeight) / n;
	double ratio = (double)systemWidth / systemHeight;

	double xFactor = sqrt(areaFactor) / ratio;
	double yFactor = areaFactor / xFactor;

	int index = 0;
	for (i = 0; i < systemWidth - 1; i += xFactor){
		if (index == n)
			break;
		for (j = 0; j < systemHeight - 1; j += yFactor){
			if (index == n)
				break;
			particles[index].xPos = i;
			particles[index].yPos = j;
			particles[index].xVel = vInitial;
			particles[index].yVel = vInitial;
			particles[index].xAccPast = ac;
			particles[index].xAccPresent = ac;
			particles[index].yAccPast = ac;
			particles[index].yAccPresent = ac;
			index++;
		}
	}

}


double getMaxVelocity(Particle particles[n]){
	double maxVel = 0;
	int i;
	for (i = 0; i < n; i++){
		if (sqrt((particles[i].xVel*particles[i].xVel) + (particles[i].yVel*particles[i].yVel)) > maxVel){
			maxVel = sqrt((particles[i].xVel*particles[i].xVel) + (particles[i].yVel*particles[i].yVel));
		}
	}
	return maxVel;
}

double getMinVelocity(Particle particles[n]){
	double minVel = LLONG_MAX;
	int i;
	for (i = 0; i < n; i++){
		if (sqrt((particles[i].xVel*particles[i].xVel) + (particles[i].yVel*particles[i].yVel)) < minVel){
			minVel = sqrt((particles[i].xVel*particles[i].xVel) + (particles[i].yVel*particles[i].yVel));
		}
	}
	return minVel;
}



int main(){

	FILE *fa, *fs;
	//std::ofstream fa, fs;
	fa = fopen("t_x_y8.txt", "w");
	fs = fopen("t_vx_vy_v.txt", "w");

	int i;
	double t;


	Particle particles[n];


	initialize(particles);


	displaceRandom(particles);


	acc(particles);					// calculating the initial forces


	for (t = tStart; t<tEnd; t = t + timeStep){
		pos(particles);				// calculating the distaces
		acc(particles);					// calculating the forces
		vel(particles);				// calculating the velocities

		//Updation of position and velocity before next time step


		for (i = 6; i < 7; i++)
		{

			printf("%f\t%d\t%f\t%f\t%f\n", t, i, particles[i].xAccPresent, particles[i].yAccPresent, sqrt((particles[i].xVel*particles[i].xVel) + (particles[i].yVel*particles[i].yVel)));

			fprintf(fa, "%le\t%le\n", particles[i].xPos, particles[i].yPos);
			fprintf(fs, "%6.2f\t%le\t%le\t%le\n", t, particles[i].xVel, particles[i].yVel, sqrt((particles[i].xVel*particles[i].xVel) + (particles[i].yVel*particles[i].yVel)));



		}

		for (i = 0; i < n; i++){

			particles[i].xAccPast = particles[i].xAccPresent;
			particles[i].yAccPast = particles[i].yAccPresent;
		}
		//	getchar();
	}
	FILE *f1;

	f1 = fopen("velocityDistribution.txt", "w");

	int maxVelocity = getMaxVelocity(particles);
	int minVelocity = getMinVelocity(particles);

	int count[21];
	for (i = 0; i<21; i++)
		count[i] = 0;

	for (i = 0; i<n; i++)
	{
		double vel = sqrt((particles[i].xVel*particles[i].xVel) + (particles[i].yVel*particles[i].yVel));
		if (vel == 0.0)
			count[0]++;
		else
			count[(int)((vel - minVelocity) * 20.0 / (maxVelocity - minVelocity)) + 1]++;
	}

	for (i = 0; i<21; i++)
		fprintf(f1, "%d      %d\n", i, count[i]);

	fclose(fa);
	fclose(fs);
	fclose(f1);
	return 0;
}
